let EngineTest = function() {};

var fs = require("fs");
var http = require("http");

EngineTest.prototype.run = (req, res, next) => {
  element = req.body;
  console.log(element);

  const { Builder, By, Key, until } = require("selenium-webdriver");

  (async function test() {
    let driver = await new Builder().forBrowser("chrome").build();

    var result = "";
    try {
      await driver.get(element.url);
      await driver.findElements(By.tagName(input)).forEach((element, index) => {
        element.sendKeys(element.lista_componentes[index], Key.RETURN);
      });
      // .findElement(By.tagName(input))
      await driver.wait(
        until.titleIs(element.valor + " - Google Search"),
        1500
      );
      // await driver.get('http://www.google.com/ncr');
      // await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
      // await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
      result = "SUCESSO";
    } catch (err) {
      result = "Teste Falhou";
      console.log("Error: " + err + ".");
    } finally {
      res.write(result);
      console.log(result);
      await driver.quit();
      res.end();
    }
  })();
};

module.exports = new EngineTest();
