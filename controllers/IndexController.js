let capturarEngine = require("../scripts/CapturarEngine");

module.exports = new (class IndexController {
  index(req, res, next) {
    res.render("index", { teste: "teste" });
  }

  capturarComponentes(req, res, next) {
    if (req.body.url !== "") {
      capturarEngine.init(req.body.url, dados => {
        res.json(JSON.stringify(dados));
      });
    }
  }
})();
