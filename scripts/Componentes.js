module.exports = class Componentes {
  constructor(test_url) {
    this.url = test_url;
    this.lista_componentes = [];
  }

  add(cTag, cType, cValue) {
    this.lista_componentes.push({
      tag: cTag,
      type: cType,
      value: cValue
    });
  }
};
