let request = require("request");
let cheerio = require("cheerio");

let Componentes = require("./Componentes");

module.exports = new (class CaputarEngine {
  init(test_url, callback) {
    request(test_url, function(err, res, body) {
      if (err) console.log("Erro: " + err);

      let componentes = new Componentes(test_url);

      let $ = cheerio.load(body);

      let inputs = $(":input");
      Array.from(inputs).forEach(element => {
        componentes.add(element.tagName, element.attribs.type, "");
      });

      return callback(componentes);
    });
  }
})();
