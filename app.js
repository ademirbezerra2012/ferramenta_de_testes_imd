let express = require("express");
let app = express();
let bodyParser = require("body-parser");
let cors = require("cors");
let path = require("path");

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(bodyParser.json());

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use("/", require("./routes"));

module.exports = app;
