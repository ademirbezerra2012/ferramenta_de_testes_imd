let express = require("express"),
  router = express.Router();

let IndexController = require("../controllers/IndexController");

let EngineTest = require("../controllers/EngineTest");

//Página inicial
router.get("/", IndexController.index);

// Engine Test
router.post("/execute-teste", EngineTest.run);

router.post("/gerar-elementos", IndexController.capturarComponentes);

module.exports = router;
